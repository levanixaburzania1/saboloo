import axios from "axios";

const SERVER_URL = "http://18.185.148.165:3000/";
const SERVER_URL_V1 = SERVER_URL + "api/v1/";

axios.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

export const removeLocalStorageItems = () => {
  localStorage.removeItem("user");
  localStorage.removeItem("token");
 
};

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response.status === 401) {
      removeLocalStorageItems ();
      if (
        window.location.pathname !== "/" &&
        window.location.pathname !== "/signup" &&
        window.location.pathname !== "/login"
      )
        window.location.href = "/";
    }
    return Promise.reject(error);
  }
);


export const login = async (email, password) => {
  try {
    const results = await axios.post(SERVER_URL + "login", { email, password });
    localStorage.setItem("user", JSON.stringify(results.data.data));
    localStorage.setItem("token", results.data.data.token);
  } catch (err) {
    throw new Error(err);
  }
};

export const register = async (
values
) => {
  try {
    await axios.post(SERVER_URL + "register", 
values
    );
  } catch (err) {
    throw new Error(err);
  }
};
 
export const fetchProducts = async () => {
  const results = await axios.get(SERVER_URL_V1 + "products");
  return results.data.data;
};

export const fetchCartProducts = async () => {
  const results = await axios.get(SERVER_URL_V1 + "cart");

  return results.data.data.cartItem.items;
};

export const addToCart = async (productId, qty) => {
  const results = await axios.post(SERVER_URL_V1 + "cart/add", {
    productId,
    qty,
  });
  return results.data.data;
};

export const removeCartProduct = async (productId) => {
  const results = await axios.post(
    SERVER_URL_V1 + `cart/remove/${productId}`,
    {}
  );

  return results;
};

export const addProduct = async (data) => {
  const results = await axios.post(SERVER_URL_V1 + "products", data);
  return results.data.data;
};

export const editProduct = async (id, data) => {
  const results = await axios.put(SERVER_URL_V1 + `products/${id}`, data);
  return results.data.data;
};

export const removeProduct = async (id) => {
  const results = await axios.delete(SERVER_URL_V1 + `products/${id}`);
  return results.data.data;
};

export const getSingleProduct = async (id) => {
  const results = await axios.get(SERVER_URL_V1 + `products/${id}`);
  return results.data.data;
};

// profile
// token validity
export const checkTokenValidity = async () => {
  try {
    const result = await axios.get(SERVER_URL_V1 + `cart`);
    return result.data;
  } catch (err) {
    throw err;
  }
};
// update user info
export const updateUserInfo = async (id, data) => {
  try {
    const result = await axios.put(SERVER_URL_V1 + `users/${id}`, data);
    return result;
  } catch (err) {
    throw err;
  }
};
