import { ActionTypes } from "../actionTypes/actionTypes";

export const setProducts = (products) => {
  return {
    type: ActionTypes.SET_PRODUCTS,
    payload: products,
  };
};

export const setSortType = (sortType, searchQuery, priceRange, products) => {
  let displayProducts = [...products];
  switch (sortType) {
    case "":
      displayProducts = displayProducts
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Price: Law To High":
      displayProducts = displayProducts
        .sort((a, b) => a.price - b.price)
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Price: High To Law":
      displayProducts = displayProducts
        .sort((a, b) => b.price - a.price)
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Name:A-Z":
      displayProducts = displayProducts
        .sort((a, b) => (a.title < b.title ? -1 : 1))
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Name:Z-A":
      displayProducts = displayProducts
        .sort((a, b) => (a.title < b.title ? 1 : -1))
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    default:
      displayProducts = [...products];
  }

  return {
    type: ActionTypes.SET_SORT_TYPE,
    payload: { sortType, displayProducts },
  };
};

export const setSearchQuery = (sortType, searchQuery, priceRange, products) => {
  let displayProducts = [...products];
  switch (sortType) {
    case "":
      displayProducts = displayProducts
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Price: Law To High":
      displayProducts = displayProducts
        .sort((a, b) => a.price - b.price)
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Price: High To Law":
      displayProducts = displayProducts
        .sort((a, b) => b.price - a.price)
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Name:A-Z":
      displayProducts = displayProducts
        .sort((a, b) => (a.title < b.title ? -1 : 1))
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Name:Z-A":
      displayProducts = displayProducts
        .sort((a, b) => (a.title < b.title ? 1 : -1))
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    default:
      displayProducts = [...products];
  }
  return {
    type: ActionTypes.SET_SEARCH_QUERY,
    payload: { searchQuery, displayProducts },
  };
};

export const setPriceRange = (sortType, searchQuery, priceRange, products) => {
  let displayProducts = [...products];
  switch (sortType) {
    case "":
      displayProducts = displayProducts
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Price: Law To High":
      displayProducts = displayProducts
        .sort((a, b) => a.price - b.price)
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Price: High To Law":
      displayProducts = displayProducts
        .sort((a, b) => b.price - a.price)
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Name:A-Z":
      displayProducts = displayProducts
        .sort((a, b) => (a.title < b.title ? -1 : 1))
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    case "Name:Z-A":
      displayProducts = displayProducts
        .sort((a, b) => (a.title < b.title ? 1 : -1))
        .filter((product) =>
          product.title.toUpperCase().includes(searchQuery.toUpperCase())
        )
        .filter((product) =>
          priceRange
            ? product.price <= Math.max(...priceRange) &&
              product.price >= Math.min(...priceRange)
            : true
        );
      break;
    default:
      displayProducts = [...products];
  }

  return {
    type: ActionTypes.SET_PRICE_RANGE,
    payload: { priceRange, displayProducts },
  };
};

export const toggleProductCheckbox = (productId) => ({
  type: ActionTypes.TOGGLE_PRODUCT_CHECKBOX,
  payload: { productId },
});


export const selectAll = (products) => {
  const selected = products.map(product=> ({...product, isChecked:true}));
  return {
    type: ActionTypes.SELECT_ALL,
    payload:selected
  };
}

export const clearAll = (products) => {
  const unselected = products.map(product=> ({...product, isChecked:false}));
  return {
    type: ActionTypes.CLEAR_ALL,
    payload:unselected
  };
}

export const setCartProducts = (cartProducts) => {
  return {
    type: ActionTypes.SET_CART_PRODUCTS,
    payload:cartProducts
}
}

export const removeProductFromCart = (cartProducts,id) => {
  const updatedCart = cartProducts.filter(product=>product.id!=id);
  return{
    type:ActionTypes.REMOVE_PRODUCT_FROM_CART,
    payload:updatedCart
  }
}

export const addProductToCart = (cartProducts,product) => {
  return {
    type:ActionTypes.ADD_PRODUCT_TO_CART,
    payload: [...cartProducts,product]
  }
}

export const editProductOfCatalog = (products,id,updatedProduct) => {
  const updatedProductList = products.map(product=> (product.id==id ? {...product,...updatedProduct} : product));
  return {
    type:ActionTypes.EDIT_PRODUCT_OF_CATALOG,
    payload:updatedProductList
  }
} 

export const removeProductFromCatalog = (products,id) => {
  const updatedProductList = products.filter(product => product.id != id);
  return {
    type:ActionTypes.REMOVE_PRODUCT_FROM_CATALOG,
    payload:updatedProductList
  }
}

export const addProductToCatalog = (products,productData) => {
  return {
    type:ActionTypes.ADD_PRODUCT_TO_CATALOG,
    payload: [...products,productData]
  }
}

