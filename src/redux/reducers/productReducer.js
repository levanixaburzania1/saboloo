import { ActionTypes } from "../actionTypes/actionTypes";

const initialState = {
  products: [],
  displayProducts: [],
  sortType: "",
  searchQuery: "",
  priceRange: [0,1000],
  selectedProducts: 0,
  cartProducts: [],
};

export const productReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_PRODUCTS:
      return { ...state, products: payload, displayProducts: payload };
    case ActionTypes.SET_SORT_TYPE:
      return {
        ...state,
        sortType: payload.sortType,
        displayProducts: payload.displayProducts,
      };
    case ActionTypes.SET_SEARCH_QUERY:
      return {
        ...state,
        searchQuery: payload.searchQuery,
        displayProducts: payload.displayProducts,
      };
    case ActionTypes.SET_PRICE_RANGE:
      return {
        ...state,
        priceRange: payload.priceRange,
        displayProducts: payload.displayProducts,
      };
    case ActionTypes.TOGGLE_PRODUCT_CHECKBOX:
      return {
        ...state,
        displayProducts: state.displayProducts.map((product) =>
          product.id === payload.productId
            ? { ...product, isChecked: !product.isChecked }
            : product
        ),
      };
    case ActionTypes.SELECT_ALL:
      return {
        ...state,
        displayProducts: payload,
      };
    case ActionTypes.CLEAR_ALL:
      return {
        ...state,
        displayProducts: payload,
      };
    case ActionTypes.SET_CART_PRODUCTS:
      return {
        ...state,
        cartProducts: payload,
      };
    case ActionTypes.REMOVE_PRODUCT_FROM_CART:
        return {
          ...state,
          cartProducts: payload,
      };  
      case ActionTypes.ADD_PRODUCT_TO_CART:
        return {
          ...state,
          cartProducts: payload,
        };
    case ActionTypes.REMOVE_PRODUCT_FROM_CATALOG:
      return { ...state, products: payload, displayProducts: payload };
    case ActionTypes.EDIT_PRODUCT_OF_CATALOG:
      return { ...state, products: payload, displayProducts: payload };
    case ActionTypes.ADD_PRODUCT_TO_CATALOG:
      return { ...state, products: payload, displayProducts: payload };
    default:
      return state;
  }
};
