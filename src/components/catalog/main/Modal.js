import "./products.css";
import { Dialog} from "@material-ui/core";
import {AiOutlineClose} from 'react-icons/ai';
import { Button} from "@material-ui/core";
import { getSingleProduct } from "../../../Api";
import {useState} from 'react';
import { useParams,useHistory } from "react-router";

const Modal = ({onAddToInventoryClick}) =>{
    const [open, setOpen] = useState(true);
    const [product,setProduct] = useState([]);
    const {productID} = useParams();
    const history = useHistory();
    getSingleProduct(productID).then(res=>setProduct(res));
    
    const handleClickOpen = () => {
      setOpen(true);
      
    };
    const handleClose = () => {
      setOpen(false);
      history.push('/catalog')
    };
    return(
<Dialog onClose={handleClose} open={open} >
        <div className="modal__content">
      <div className="modal__left-side">
          <div className="modal__price">COST: ${product.price}</div>
          <div className="modal__photo">
            <img src={product.imageUrl} />
          </div>
        </div>
        <div className="modal__right-side">
          <h3 className="modal__title">{product.title}</h3>
          
            <Button className="modal__btn" onClick={onAddToInventoryClick}>ADD TO INVENTORY</Button>
          
          <h4 className="modal__description-title">description</h4>
          <hr />
          <p className="modal__description">{product.description}</p>
        </div>
        <div className="close-modal" onClick={handleClose}>
          <AiOutlineClose/>
        </div>
        </div>
      </Dialog>
    )
}

export default Modal;