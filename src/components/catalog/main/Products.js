import './products.css';
import {useEffect } from "react";
import { fetchProducts } from "../../../Api";
import SingleProducts from "./SingleProducts";
import { useSelector, useDispatch } from 'react-redux';
import {setProducts} from '../../../redux/actions/productActions';


const Products = () => {


const dispatch = useDispatch();   
const productList = useSelector(state => state.allProducts.displayProducts);

useEffect( ()=>{
        fetchProducts().then(res=>dispatch(setProducts(res)));
        
    },[])    




    
    return(
        <div className="main">
        {productList && productList.map(product=><SingleProducts  product={product} key={product.id}/>)}
        </div>
    )
}

export default Products;