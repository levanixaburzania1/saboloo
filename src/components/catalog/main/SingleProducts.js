import "./products.css";
import { useState } from "react";
import { Button, Checkbox } from "@material-ui/core";
import { addToCart, fetchCartProducts, editProduct } from "../../../Api";
import { AiFillCheckCircle } from "react-icons/ai";
import { MdRadioButtonUnchecked } from "react-icons/md";
import { useDispatch,useSelector } from "react-redux";
import { toggleProductCheckbox,setCartProducts } from "../../../redux/actions/productActions";
import { useHistory, useParams } from "react-router-dom";
import Modal from "./Modal";
import { AiOutlineEdit } from "react-icons/ai";
import { removeProductFromCatalog,editProductOfCatalog,addProductToCart } from "../../../redux/actions/productActions";

import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import {AiOutlineClose} from 'react-icons/ai';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import {removeProduct} from '../../../Api';
import { useSnackbar } from "notistack";









const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
      display:'flex',
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
      
    },
 
  },
  textField:{
    margin:'10px'
  },

}));



const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },

});



const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    width:850,
    height:400
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <AiOutlineClose />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const SingleProducts = ({ product }) => {

  const products = useSelector(state => state.allProducts.displayProducts);
  const cartProducts = useSelector(state => state.allProducts.cartProducts);

  const classes = useStyles();
  
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };


  const handleClose = () => {
    setOpen(false);
  }


  const [showProductHead, setShowProductHead] = useState(false);
  const { productID } = useParams();
  const [qty, setQty] = useState(1);
  const onQtyDecrease = () => {
    setQty(qty + 1);
  };
  const onQtyIncrease = () => {
    if (qty != 1) setQty(qty - 1);
  };

  const dispatch = useDispatch();

  const handleChangeCheckBox = (productId) => {
    console.log("produtctID", productId);
    dispatch(toggleProductCheckbox(productId));
  };
  const history = useHistory();
  const openModal = () => {
    history.push(`/catalog/${product.id}`);
  };

  // edit and remove



  const [data,setData] = useState({
    title:product.title,
    description:product.description,
    price:product.price,
    imageUrl:product.imageUrl
  });
  const { enqueueSnackbar } = useSnackbar();
  const handleEdit = () => {
    
    editProduct(product.id,data).then(()=>{
      dispatch(editProductOfCatalog(products,product.id,data));
      enqueueSnackbar("Information has been Updated successfully", {
        variant: "success",
        autoHideDuration: 1000,
      });
    }).catch((err)=>{
      enqueueSnackbar(err, {
        variant: "error",
        autoHideDuration: 2000,
      });
    })

    setOpen(false);
  };
  const handleRemove = () => {
    
    removeProduct(product.id).then(()=>{
      dispatch(removeProductFromCatalog(products,product.id));
      enqueueSnackbar("product has been Deleted successfully", {
        variant: "success",
        autoHideDuration: 1000,
      });
    }).catch((err)=>{
      enqueueSnackbar(err, {
        variant: "error",
        autoHideDuration: 2000,
      })
    })
    
    setOpen(false);
  };

  const onAddToInventoryClick = ()=>{
    addToCart(product.id, qty).then(()=>{
      dispatch(addProductToCart(cartProducts,product));
      enqueueSnackbar("product has been Added to Inventory", {
        variant: "success",
        autoHideDuration: 1000,
      });
    }).catch((err)=>{
      enqueueSnackbar(err, {
        variant: "error",
        autoHideDuration: 2000,
      })
    });
    
  }
  return (
    <div
      className="product"
      onMouseEnter={() => {
        setShowProductHead(true);
      }}
      onMouseLeave={() => {
        setShowProductHead(false);
      }}
    >
      <div className="product__img">
        <img src={product.imageUrl} onClick={openModal} />
      </div>
      <h3 className="product__title">{product.title}</h3>
      <div className="product__price">price: ${product.price}</div>
      {(product.isChecked ? true : showProductHead) && (
        <div className="product__head">
          <Checkbox
            checkedIcon={
              <AiFillCheckCircle
                style={{ color: "rgb(97,213,223)", fontSize: "30px" }}
              />
            }
            icon={
              <MdRadioButtonUnchecked
                style={{ color: "gray", fontSize: "30px" }}
              />
            }
            color="primary"
            checked={Boolean(product.isChecked)}
            onChange={() => handleChangeCheckBox(product.id)}
          />
          {JSON.parse(localStorage.getItem("user")) && JSON.parse(localStorage.getItem("user")).isAdmin && (
            <AiOutlineEdit
              style={{ marginLeft: "-110px" }}
              onClick={handleClickOpen}
            />
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={onAddToInventoryClick}
          >
            Add to Inventory
          </Button>
        </div>
      )}
      <div id="quantity-wrapper">
        <Button onClick={onQtyIncrease}>-</Button>
        <p>{qty}</p>
        <Button onClick={onQtyDecrease}>+</Button>
      </div>
      {productID == product.id && <Modal onAddToInventoryClick={onAddToInventoryClick}/>}
  
  
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open} maxWidth="xl">
      <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          edit/remove product
        </DialogTitle>
        <DialogContent dividers>
        <form className={classes.root} noValidate autoComplete="off" >
      <div><TextField className={classes.textField}  label="title" variant="outlined" defaultValue={product.title} onChange={(e)=>setData({...data, title:e.target.value})}/></div>
      <div><TextField className={classes.textField}  label="description" variant="outlined" defaultValue={product.description} onChange={(e)=>setData({...data, description:e.target.value})}/></div>
      <div><TextField className={classes.textField}  label="price" variant="outlined" defaultValue={product.price} onChange={(e)=>setData({...data, price:e.target.value})}/></div>
      <div><TextField className={classes.textField}  label="imageUrl" variant="outlined" defaultValue={product.imageUrl} onChange={(e)=>setData({...data, imageUrl:e.target.value})}/></div>
    </form>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleEdit} color="primary">
            edit
          </Button>
          <Button autoFocus onClick={handleRemove} color="secondary">
            remove
          </Button>
        </DialogActions>
      </Dialog>


    </div>
  );
};
export default SingleProducts;