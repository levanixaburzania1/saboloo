import "./inventory.css";
import { fetchCartProducts } from "../../../../Api";
import { useEffect, useState } from "react";

import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { IoIosRemoveCircle } from "react-icons/io";
import { removeCartProduct } from "../../../../Api";
import { setCartProducts } from "../../../../redux/actions/productActions";
import { useDispatch,useSelector } from "react-redux";
import { removeProductFromCart } from "../../../../redux/actions/productActions";
import { useSnackbar } from "notistack";

const Inventory = () => {
  const dispatch = useDispatch();
  const displayCartProducts = useSelector(state=>state.allProducts.cartProducts);
  const len = displayCartProducts.length;
  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: 'rgb(56, 105, 188)',
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

  const StyledTableRow = withStyles((theme) => ({
    root: {
      "&:nth-of-type(odd)": {
        backgroundColor: 'rgb(225, 230, 239)',
      },
    },
  }))(TableRow);

  function createData(id, image, title, qty, price) {
    return { id, image, title, qty, price };
  }

  const rows =
  displayCartProducts &&
  displayCartProducts.map((product) =>
      createData(
        product.id,
        product.image,
        product.title,
        product.qty,
        product.price
      )
    );

  const useStyles = makeStyles({
    table: {
      width:'95%',
      minWidth: 700,
      margin: "auto",
    },
  });
  

  useEffect(() => {
    fetchCartProducts().then(res=>dispatch(setCartProducts(res)))
    
  },[]);

  

  const classes = useStyles();
  const {enqueueSnackbar} = useSnackbar();
  
  return (
    <>
      <h1 className="cart-title">inventory</h1>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
        {len ?  <>         <TableHead>
            <TableRow>
              <StyledTableCell>id</StyledTableCell>
              <StyledTableCell>image</StyledTableCell>
              <StyledTableCell align="right">title</StyledTableCell>
              <StyledTableCell align="right">quantity</StyledTableCell>
              <StyledTableCell align="right">price</StyledTableCell>
              <StyledTableCell align="center">remove</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <StyledTableRow key={row.id}>
                <StyledTableCell align="left">{row.id}</StyledTableCell>
                <StyledTableCell component="th" scope="row">
                  <img
                    src={row.image}
                    alt="product-image"
                    style={{ width: "60px" }}
                  />
                </StyledTableCell>
                <StyledTableCell align="right">{row.title}</StyledTableCell>
                <StyledTableCell align="right">{row.qty}</StyledTableCell>
                <StyledTableCell align="right">
                  {row.price * row.qty} $
                </StyledTableCell>
                <StyledTableCell
                  align="center"
                >
                  <IoIosRemoveCircle className="remove-btn" onClick={() => {
                    removeCartProduct(row.id).then(()=>{
                      dispatch(removeProductFromCart(displayCartProducts,row.id));
                      enqueueSnackbar("product has been removed successfully", {
                        variant: "success",
                        autoHideDuration: 1000,
                      });
                    }).catch((err)=>{
                      enqueueSnackbar("Information hasn't been Updated successfully", {
                        variant: "error",
                        autoHideDuration: 1000,
                      });
                    })

                  }}/>
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody> </> : <img src="https://lh3.googleusercontent.com/proxy/rz1mBMd0UsNm_WLhfxgFEfvl1Kf-rPwMo6d5TSNLpBLevSETCtZzjiLa5rxZLNjJSje_a_wQhzT2RcR0VMVCifAHLdHmNh9FhVivM9g" alt="empty"/>}


        </Table>
      </TableContainer>
    </>
  );
};

export default Inventory;
