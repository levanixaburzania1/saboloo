import './mainNav.css'
import {FaUserCircle,  FaCube, FaShoppingCart} from 'react-icons/fa' ;
import {AiOutlineUnorderedList} from 'react-icons/ai';
import {IoIosSpeedometer,IoMdAddCircle} from 'react-icons/io';
import {CgArrowsExchangeAlt} from 'react-icons/cg';
import {BsCardList} from 'react-icons/bs';
import { useHistory } from 'react-router';


const MainNav = () => {
    const history = useHistory();
    const onInventoryClick = () => {
    localStorage.getItem('token') ? history.push('/inventory') : history.push('/login')
    }
    const onAddProductClick = () => {
        history.push('/add-product')
            }
    const onEditProductClick = () =>{
        history.push('/edit-product')         
    }
    
    const onProfileClick = () => {
        history.push('/profile');
    }

    return(
        <div className="main-nav">
       <img src="/images/Logo-365dropship.jpg" alt="logo"/>     
       <div class="main-nav__btn"><FaUserCircle onClick={onProfileClick} style={{borderRadius:"50%", padding:"2px", border:"2px solid #61d5df"}} /></div>
       <div class="main-nav__btn">< IoIosSpeedometer/></div>
       <div class="main-nav__btn main-nav__btn--selected"><AiOutlineUnorderedList /> </div>
       <div class="main-nav__btn"><FaCube  /> </div>
       <div class="main-nav__btn" onClick={onInventoryClick}><FaShoppingCart/></div>
       <div class="main-nav__btn" ><IoMdAddCircle /> </div>
       <div class="main-nav__btn" ><CgArrowsExchangeAlt/> </div>
       <div class="main-nav__btn"> <BsCardList /></div>


      </div>
    )
}

export default MainNav;