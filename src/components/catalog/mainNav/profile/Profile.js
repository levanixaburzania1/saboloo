import "./profile.css";
import { Button, Input } from "@material-ui/core";
import { Formik, Field, Form, ErrorMessage } from "formik";
import { useState, useEffect } from "react";
import { checkTokenValidity } from "../../../../Api";
import { useHistory } from "react-router";
import * as yup from "yup";
import { useSnackbar } from "notistack";
import { updateUserInfo } from "../../../../Api";

const updateUserInfoSchema = yup.object().shape({
  firstName: yup.string().required("first Name is required"),
  lastName: yup.string().required("last Name is required"),
  email: yup.string().email().required(),
  password: yup
    .string()
    .required("Password is required")
    .min(6, "Minimum 6 characters required"),
});

const Profile = () => {
  const history = useHistory();
  const [user, setUser] = useState({ firstName: "", lastName: "", email: "" });
  const { enqueueSnackbar } = useSnackbar();

  const fillUpUserState = () => {
    setUser({
      firstName: JSON.parse(localStorage.getItem("user")).firstName,
      lastName: JSON.parse(localStorage.getItem("user")).lastName,
      email: JSON.parse(localStorage.getItem("user")).email,
    });
  };

  useEffect(() => {
    checkTokenValidity();
  }, []);

  useEffect(() => {
    fillUpUserState();
  }, []);

  const handleSubmit = (values) => {
    const userId = JSON.parse(localStorage.getItem("user")).id;
    updateUserInfo(userId, values)
      .then((result) => {
        localStorage.setItem("user", JSON.stringify(result.data.data));
        fillUpUserState();
        enqueueSnackbar("Information has been Updated", {
          variant: "success",
          autoHideDuration: 1000,
        });
        history.push("/catalog");
      })
      .catch((err) => {
        enqueueSnackbar("Information hasn't been Updated successfully", {
          variant: "error",
          autoHideDuration: 1000,
        });
      });
  };
  return (
    <div className="profile">
        
      <div className="profile__header">
        <h4>MY PROFILE</h4>
        <Button style={{  backgroundColor:'#61d5df',
        color:'white',
        marginRight:'40px'}} onClick={()=>{history.push('/')}}>home</Button>
      </div>
      <div className="profile__content">
      <div className="profile__main">
        <Formik
          enableReinitialize
          initialValues={{
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            password: "",
            
          }}
          onSubmit={handleSubmit}
          validationSchema={updateUserInfoSchema}
        >
          <Form className="personal-info__form">
            <div className="containers-wrapper">
              <h6 className="user-info__heading">PERSONAL DETAILS</h6>
              <div className="user-info__body">
                <div className="input__container">
                  <label className="firstName input-label">First Name</label>
                  <Field
                    className="personal-info__input"
                    name="firstName"
                    variant="outlined"
                  />
                  <ErrorMessage
                    component="span"
                    name="firstName"
                    className="personal-info__error-message"
                  />
                </div>
                <div className="input__container">
                  <label className="lastName input-label">Last Name</label>
                  <Field
                    className="personal-info__input"
                    name="lastName"
                    variant="outlined"
                  />
                  <ErrorMessage
                    component="span"
                    name="lastName"
                    className="personal-info__error-message"
                  />
                </div>
                <div className="input__container">
                  <label className="email input-label">Email</label>
                  <Field
                    className="personal-info__input"
                    name="email"
                    variant="outlined"
                  />
                  <ErrorMessage
                    component="span"
                    name="email"
                    className="personal-info__error-message"
                  />
                </div>
                <div className="input__container">
                  <label className="new-password input-label">
                    New password
                  </label>
                  <Field
                    className="personal-info__input"
                    name="password"
                    type="password"
                  />
                  <ErrorMessage
                    component="span"
                    name="password"
                    className="personal-info__error-message"
                  />
                </div>
              </div>
            </div>

            <input  type="submit" className="save-changes-btn" value="save changes"/>
             
          </Form>
        </Formik>
      </div>
    
    </div>
    </div>
  );
};

export default Profile;
