import { FiSearch } from "react-icons/fi";
import "./nav.css";
import { useState } from "react";
import { useSelector,useDispatch } from "react-redux";
import { setSearchQuery } from "../../../../redux/actions/productActions";

const Nav = () => {
  const dispatch = useDispatch();

  
  const sortType = useSelector(state => state.allProducts.sortType);
  const priceRange = useSelector(state=>state.allProducts.priceRange); 
  const products = useSelector(state => state.allProducts.products);

  const [searchValue, setSearchValue] = useState("");

  const onSearchValueChange = (e) => {
    setSearchValue(e.target.value);
  };
  const onSearch = () => {
    dispatch(setSearchQuery(sortType,searchValue,priceRange,products));
  };

  return (
    <div className="nav">
      <input
        className="nav__search-input"
        type="text"
        placeholder="search..."
        value={searchValue}
        onChange={onSearchValueChange}
      />
      <FiSearch className="search-btn" onClick={onSearch} />
    </div>
  );
};

export default Nav;
