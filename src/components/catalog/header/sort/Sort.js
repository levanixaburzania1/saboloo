import './sort.css';
import {MdSort} from 'react-icons/md';
import {TiArrowUnsorted} from 'react-icons/ti';
import { setSortType } from '../../../../redux/actions/productActions';
import { useSelector, useDispatch } from 'react-redux';
import {useState} from 'react';
import {Button} from '@material-ui/core';
import { addProductToCatalog } from '../../../../redux/actions/productActions';

import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import {AiOutlineClose} from 'react-icons/ai';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { addProduct } from '../../../../Api';
import { useSnackbar } from "notistack";

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
      display:'flex',
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
      
    },
 
  },
  textField:{
    margin:'10px'
  }
}));

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <AiOutlineClose />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);


const Sort = () => {
  const classes = useStyles();
  const [data,setData] = useState({
    title:"",
    description:"",
    price:0,
    imageUrl:""
  });
  const products = useSelector(state => state.allProducts.products);
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const { enqueueSnackbar } = useSnackbar();
  const handleAddProduct = () => {
    addProduct(data).then((res)=>{
      dispatch(addProductToCatalog(products,res))
      enqueueSnackbar("Product has been Added sucessfully", {
        variant: "success",
        autoHideDuration: 1000,
      });
    }).catch((err)=>{
      enqueueSnackbar("Information hasn't been Updated successfully", {
        variant: "error",
        autoHideDuration: 1000,
      });
    })
    setOpen(false);
  }

      const [showSortOptions, setSortOptions] = useState(false);
      const dispatch = useDispatch();

      
      const searchQuery = useSelector(state=>state.allProducts.searchQuery);
      const priceRange = useSelector(state=>state.allProducts.priceRange);  

      const sortDropdown = () => {
        setSortOptions(showSortOptions ? false : true);
      };
      const [currentSort, setCurrentSort] = useState("New Arrival");
      const onSortOptionClick = (e) => {
        setCurrentSort(e.target.innerHTML);
        dispatch(setSortType(e.target.innerHTML,searchQuery,priceRange,products));
      };

    return(
        
      <div className="sort__select">
        <div className="sort" onClick={sortDropdown}>
          <div className="sort__head">
            {" "}
            <MdSort className="sort-logo" />
            Sort By: <span className="sort-type">{currentSort}</span>
            <TiArrowUnsorted />
          </div>
          {showSortOptions && (
            <div class="sort__options">
              <div className="sort__option" onClick={onSortOptionClick} >
                Price: Law To High
              </div>
              <div className="sort__option" onClick={onSortOptionClick}>
                Price: High To Law
              </div>
              <div className="sort__option" onClick={onSortOptionClick}>
                Name:A-Z
              </div>
              <div className="sort__option" onClick={onSortOptionClick}>
                Name:Z-A
              </div>
            </div>
          )}
        </div>
        <div>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          add product
        </DialogTitle>
        <DialogContent dividers>
        <form className={classes.root} noValidate autoComplete="off" >
      <div><TextField className={classes.textField} label="title" variant="outlined"  onChange={(e)=>setData({...data, title:e.target.value})}/></div>
      <div><TextField className={classes.textField} label="description" variant="outlined"  onChange={(e)=>setData({...data, description:e.target.value})}/></div>
      <div><TextField className={classes.textField} label="price" variant="outlined"  onChange={(e)=>setData({...data, price:e.target.value})}/></div>
      <div><TextField className={classes.textField} label="imageUrl" variant="outlined"  onChange={(e)=>setData({...data, imageUrl:e.target.value})}/></div>
    </form>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleAddProduct} color="primary">
            add
          </Button>
        </DialogActions>
      </Dialog>
    </div>
        {JSON.parse(localStorage.getItem("user")) && (JSON.parse(localStorage.getItem("user")).isAdmin && <Button id="add_product-btn" onClick={handleClickOpen}>add product</Button>)}
      </div>

        
    )
}

export default Sort;