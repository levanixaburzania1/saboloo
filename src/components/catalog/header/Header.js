import Nav from './nav/Nav';
import Sort from './sort/Sort';
import { Button } from "@material-ui/core";
import { BsQuestionCircle } from "react-icons/bs";
import './header.css';
import {useSelector,useDispatch} from 'react-redux';
import { selectAll,clearAll } from '../../../redux/actions/productActions';
import { useState } from 'react';

const Header = () => {
  // const productsQty = useSelector(state=>state.allProducts.displayProducts.length);
  const products = useSelector(state => state.allProducts.displayProducts);
  const dispatch = useDispatch();
  const onSelectAllClick = () => {
    dispatch(selectAll(products))
    
  }
  const selectedItemsQty = products.filter(product=>product.isChecked == true).length;
  const onClearAllClick = () => {
    dispatch(clearAll(products))
  }
  
  
  return (
    <header className="header">
      <div className="header__raw1">
        <div className="header__left">
          <Button className="header__btn" onClick={onSelectAllClick}><p className="btn-text">SELECT ALL</p></Button>
          <span className="seperator"></span>
          <span className="header__selected-items">
      <span className="header__selected-items--hidden">
        selected <span className="selected-items__quantity">{selectedItemsQty}</span>{" "}
        out of
      </span>{" "}
      {products.length} products
    </span>
          <Button className="header__btn" display="none" onClick={onClearAllClick}><p className="btn-text">CLEAR</p></Button>
        </div>
        <div className="header__right">
          <Button className="header__btn"><p className="btn-text">ADD TO INVENTORY</p></Button>
          <Nav/>
            <BsQuestionCircle className="header__help-btn"/>
          
        </div>
      </div>
      <div className="header__raw2">
        <Sort/>
      </div>
    </header>
  );
};

export default Header;
