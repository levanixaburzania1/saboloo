import React from "react";
import Slider from "@material-ui/core/Slider";
import "./priceRange.css";
import { setPriceRange } from "../../../../redux/actions/productActions";
import { useSelector,useDispatch } from "react-redux";

const PriceRange = () => {
  const [value, setValue] = React.useState([0, 1000]);
  const dispatch = useDispatch();

  
  const sortType = useSelector(state => state.allProducts.sortType);
  const searchQuery = useSelector(state=>state.allProducts.searchQuery);
  const products = useSelector(state => state.allProducts.products);

  

  const handleChange = (event, newValue) => {
    setValue(newValue)
 
      dispatch(setPriceRange(sortType,searchQuery,newValue,products));

    
    
  }

  return (
    <div className="priceRange">
      <Slider
        max={1000}
        value={value}
        onChange={handleChange}
        aria-labelledby="continuous-slider"
      />
      <div className="show-price">
        <div className="price">{Math.min(...value)} $</div>
        <div className="price">{Math.max(...value)} $</div>
      </div>
    </div>
  );
};

export default PriceRange;
