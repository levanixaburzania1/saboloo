import "./sidebar.css";
import Category from './category/Category';
import PriceRange from './priceRange/PriceRange';

const Sidebar = () => {


  return (
<div className="sidebar">
<Category categoryType="niche"/>  
<Category categoryType="category" backgroundColor={"light_select"}/>
<PriceRange/>
</div>
  );
};

export default Sidebar;