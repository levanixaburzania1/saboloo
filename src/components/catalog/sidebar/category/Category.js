import { useState,useEffect } from "react";
import { RiArrowDownSLine } from "react-icons/ri";
import './category.css';

const Category = ({categoryType,backgroundColor}) => {
  const [showOptions, setShowOptions] = useState(false);
  const categories = [
    "electronics",
    "jewelery",
    "men's clothing",
    "women's clothing",
  ];

  const niche = [];

  const [options, setOptions] = useState();
useEffect(()=>{
    if(categoryType === "category"){
        setOptions(categories);
    } else if (categoryType === "niche") {
        setOptions(niche);
    }
},[])

  return (
    <div className="category-select">
      <div
        
        className="select-title"
        id = {backgroundColor}
        
        onClick={() => setShowOptions(showOptions ? false : true)}
      >
        <span>choose a {categoryType}</span>
        <RiArrowDownSLine />
      </div>
      {showOptions && options.map(option => <div className="category-option">{option}</div>)}
    </div>
  );
};

export default Category;
