import Header from './header/Header';
import Sidebar from './sidebar/SideBar';
import MainNav from './mainNav/MainNav';
import Products from './main/Products'
const Catalog = () => {
    return(
        <>
        <Header/>
        <Sidebar/>
        <Products/>
        <MainNav/>
        </>
    )
}

export default Catalog;