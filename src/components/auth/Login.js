import './auth.css';
import {useEffect, useState} from 'react';
import {login} from '../../Api';
import {useHistory} from 'react-router-dom';

const Login = ({onLogin}) => {

const history = useHistory();
const [email,setEmail] = useState("");
const [password,setPassword] = useState("");

const onEmailChange = (e) => {
    setEmail(e.target.value)
}

const onPasswordChange = (e) => {
    setPassword(e.target.value)
}

const performLogin = (e) => {
    e.preventDefault();
    login(email,password).then(()=>{
        loginSuccess();
        onLogin(true);
    }).catch(err=>alert("არასწორია რაღაც"))
}
useEffect(()=>{
    if(localStorage.getItem('token')){loginSuccess()}
},[])
const loginSuccess = () => {
    history.push("/")
}
    return(
        <div className="login">
        <form className="form" onSubmit={performLogin}>
            <h2 style={{fontSize:'25px',margin:'10px 0px'}}>Login</h2>
        <input 
        type="email"
        name="email"
        placeholder="enter email"
        value={email}
        onChange={onEmailChange}
        required
         />
        <input type="password"
         name="password"
         placeholder="enter password"
         value={password}
         onChange={onPasswordChange}
         required
         />
        <input type="submit" value="Login" />
        </form>
        </div>
    )
}

export default Login;