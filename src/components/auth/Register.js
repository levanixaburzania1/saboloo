import { useState } from "react";
import { register } from "../../Api";
import * as yup from "yup";
import { useSnackbar } from "notistack";
import PersonIcon from "@material-ui/icons/Person";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import EmailIcon from "@material-ui/icons/Email";
import {TextField, Button, InputAdornment } from "@material-ui/core";
import { Formik, Form } from "formik";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/styles";
export const useStyles = makeStyles({
register:{
  
  height:'100vh',
  marginTop:'0px',
  paddingTop:'100px',
  backgroundImage:'url("https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/08/lasthero.png")',
  backgroundPosition: 'center',
  backgroundSize: 'cover'
}
,
  outlined: {
    marginRight: "5px",
    color: "#61d5df",
    width: "120px",
    fontSize: "16px",
    border: "2px solid #61d5df",
    "&:Hover": {
      backgroundColor: "#61d5df",
      color: "#fff",
    },
  },
  signUp: {
    width: "170px",
    height: "50px",
    color: "#fff",
    backgroundColor: "#61d5df",
    marginTop: "-50px",
    alignSelf: "center",
    "&:Hover": {
      backgroundColor: "#61d5df",
    },
  },
  textField: {
    width: "80%",
    maxWidth:600,
    height: "20px",
    marginBottom: "50px",
  },
  submitButton:{
      marginTop: "10px",
      width: "100px",
      height: "50px",
      backgroundColor: "#61d5df",
      color: "#fff",
      "&:Hover": {
        backgroundColor: "#61d5df",
      },
  },
form:{
  display:'flex',
  flexDirection:'column',
  width:'100%',
  maxWidth:'600px',
  justifyContent:'center',
  alignItems:'center',
  margin:'auto',
  backgroundColor:'white',
  padding:'40px 0px',
  borderRadius:'10px'
}
});



const registrationSchema = yup.object().shape({
  firstName: yup.string().required("First name is required"),
  lastName: yup.string().required("Last name is required"),
  email: yup.string().email("Incorrect Email").required("Email is required"),
  password: yup
    .string()
    .required("Password is required")
    .min(6, "Minimum 6 characters required"),
  passwordConfirmation: yup
    .string()
    .oneOf([yup.ref("password")], "Passwords don't match ")
    .required("Password confirmation is required"),
});


const Register = () => {
const classes = useStyles();
const {enqueueSnackbar} = useSnackbar();
const history = useHistory();
const [serverError,setServerError] = useState(false);

const performRegister = (values)=>{
    register(values).then(()=>{
      enqueueSnackbar("registration sucessfull", {
        variant: "success",
        autoHideDuration: 1000,
      });
        setTimeout(()=>{
           history.push('/login')},1000)}).catch(err=>{alert(err)
          }).catch(err=>
            enqueueSnackbar("registration is not sucessfull", {
              variant: "error",
              autoHideDuration: 1000,
            }))
          
}



  return (
    <div className={classes.register}>
    <Formik
    enableReinitialize
    initialValues={{
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      passwordConfirmation: "",
    }}
    onSubmit={performRegister}
    validationSchema={registrationSchema}
    
  >
    {({ values, handleChange, handleBlur, touched, errors }) => {
      return (
    <Form className="form__inputs" className={classes.form}>
      <h1 style={{fontSize:'30px',marginBottom:'40px'}}>register</h1>
                  <TextField
                    type="text"
                    name="firstName"
                    error={errors.firstName && touched.firstName}
                    label={
                      errors.firstName && touched.firstName
                        ? errors.firstName
                        : "First Name"
                    }
                    variant="outlined"
                    onBlur={handleBlur}
                    value={values.firstName}
                    onChange={handleChange}
                    className={classes.textField}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <PersonIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <TextField
                    type="text"
                    name="lastName"
                    error={errors.lastName && touched.lastName}
                    label={
                      errors.lastName && touched.lastName
                        ? errors.lastName
                        : "Last Name"
                    }
                    variant="outlined"
                    onBlur={handleBlur}
                    value={values.lastName}
                    onChange={handleChange}
                    className={classes.textField}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <PersonIcon />
                        </InputAdornment>
                      ),
                    }}
                  />

                  <TextField
                    type="email"
                    name="email"
                    error={errors.email && touched.email}
                    label={
                      errors.email && touched.email ? errors.email : "Email"
                    }
                    variant="outlined"
                    onBlur={handleBlur}
                    value={values.email}
                    onChange={handleChange}
                    className={classes.textField}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <EmailIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <TextField
                    type="password"
                    name="password"
                    error={errors.password && touched.password}
                    label={
                      errors.password && touched.password
                        ? errors.password
                        : "Password"
                    }
                    variant="outlined"
                    onBlur={handleBlur}
                    value={values.password}
                    onChange={handleChange}
                    className={classes.textField}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <VpnKeyIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <TextField
                    type="password"
                    name="passwordConfirmation"
                    error={
                      errors.passwordConfirmation &&
                      touched.passwordConfirmation
                    }
                    label={
                      errors.passwordConfirmation &&
                      touched.passwordConfirmation
                        ? errors.passwordConfirmation
                        : "Password Confirmation"
                    }
                    variant="outlined"
                    value={values.passwordConfirmation}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    className={classes.textField}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <VpnKeyIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                  {serverError && (
                    <p className="server__error">Account already exists</p>
                  )}
                  <Button className={classes.submitButton} type="submit">
                    Sign Up
                  </Button>
                </Form>
              );
            }}
          </Formik>
          </div>
  );
};

export default Register;
