import "./App.css";
import { Route, Switch, Link } from "react-router-dom";
import Catalog from "./components/catalog/Catalog";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import Inventory from "./components/catalog/mainNav/inventory/Inventory";
import { Button } from "@material-ui/core";
import { useEffect, useState } from "react";
import Profile from "./components/catalog/mainNav/profile/Profile";

function App() {
  const [logged, setLogged] = useState(false);
  const onLogin = (e) => {
    setLogged(e);
  };
  useEffect(() => {
    if (localStorage.getItem("token")) {
      setLogged(true);
    }
  }, [localStorage.getItem("token")]);

  const logOut = () => {
    localStorage.clear();
    setLogged(false);
  };

  return (
    <div className="App">
      <Switch>
        <Route exact path="/">
          <div className="home">
            <div className="home__header">
              <Link to="/"></Link>
              <Link to="/catalog" className="home__btn">
                <Button
                  variant="contained"
                  style={{ backgroundColor: "#61d5df", color: "white" }}
                >
                  catalog
                </Button>
              </Link>
              {logged ? (
                <>
                  <p style={{ color: "white" }}>
                    {JSON.parse(localStorage.getItem("user")).email}
                  </p>
                  <Button
                    style={{
                      backgroundColor: "#61d5df",
                      color: "white",
                      marginLeft: "10px",
                    }}
                    onClick={logOut}
                  >
                    Log Out
                  </Button>
                </>
              ) : (
                <>
                  <Link to="/login" className="home__btn">
                    <Button
                      variant="contained"
                      style={{ backgroundColor: "#61d5df", color: "white" }}
                    >
                      login
                    </Button>
                  </Link>
                  <Link to="/signup" className="home__btn">
                    <Button
                      variant="contained"
                      style={{ backgroundColor: "#61d5df", color: "white" }}
                    >
                      sign up
                    </Button>
                  </Link>
                </>
              )}
            </div>
            <div className="home__main">
              <div className="main__logo">
                <img
                  className="logo__img"
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg"
                  alt="logo"
                />
                <h3 className="logo__text">WE GOT YOUR SUPPLY CHAIN COVERED</h3>
                <h3 className="logo__text">365 DAYS A YEAR</h3>
              </div>
            </div>
          </div>
        </Route>
        <Route exact path="/catalog">
          <Catalog />
        </Route>
        <Route exact path="/login">
          <Login onLogin={onLogin} />
        </Route>
        <Route exact path="/signup">
          <Register />
        </Route>
        <Route exact path="/inventory">
          <Inventory />
        </Route>
        <Route exact path="/catalog/:productID">
          <Catalog />
        </Route>
        <Route exact path="/profile">
          <Profile />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
